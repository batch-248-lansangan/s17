console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function yourName(){
	let fullName = prompt("What's your name?");
	

	console.log("Hello, " + fullName);

}

yourName();

function currentAge(){
	let legalAge = prompt("What is your age?");

	console.log("You are " + legalAge + " years old.");
}

currentAge();

function currentAddress(){
	let myAddress = prompt("Where are you from?");

	console.log("You are from " + myAddress);
}

currentAddress();

function myBand(){
	let bandName1 = "1.Ben and Ben";
	let bandName2 = "2.December Avenue";
	let bandName3 = "3.Calla Lily";
	let bandName4 = "4.Kamikazee";
	let bandName5 = "5.Urbandub";
	console.log("My Favorite Bands are:");
	console.log(bandName1);
	console.log(bandName2);
	console.log(bandName3);
	console.log(bandName4);
	console.log(bandName5);

}

myBand();

function myMovies(){

	let movie1 = "1. The Dark Knight";
	let rating1 = "Rotten Tomatoes ratings: 94%";
	let movie2 = "2. The Dark Knight Rises";
	let rating2 = "Rotten Tomatoes ratings: 87%";
	let movie3 = "3. Limitless";
	let rating3 = "Rotten Tomatoes ratings: 87%";
	let movie4 = "4. Joker";
	let rating4 = "Rotten Tomatoes ratings: 69%";
	let movie5 = "5. Doctor Strange in the Multiverse of Madness";
	let rating5 = "Rotten Tomatoes ratings: 74%";
	console.log("My Favorite Movies are:");
	console.log(movie1);
	console.log(rating1);
	console.log(movie2);
	console.log(rating2);
	console.log(movie3);
	console.log(rating3);
	console.log(movie4);
	console.log(rating4);
	console.log(movie5);
	console.log(rating5);

} 

myMovies();





myFriends();
function myFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}



